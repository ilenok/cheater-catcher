import vecutils
import os

import numpy as np
import scipy.cluster.hierarchy as hcluster


import pylab
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

#countries = ["Russia"]


countries = ["Russia" , "Ukraine", "Belarus", "Kazakhstan","Turkey"]
scores = ['VITAL_CANDIDATE', 'USEFUL', 'RELEVANT_PLUS', 'RELEVANT_MINUS', 'IRRELEVANT', '_404', 'VIRUS']

n_iter = 20
step = 1.0 / n_iter

output_dir_name = "clustering/hclusters"
if not os.path.exists(output_dir_name):
	os.makedirs(output_dir_name)
	

for c in countries:
	g = open(output_dir_name + "/" + c + "_nclusters.txt", "a")	
	for k in xrange(1, n_iter + 1):	
		d = step * k
		print c, str(d)
		
		pictures_dir_name = output_dir_name + "/pictures_" + str(d)
		if not os.path.exists(pictures_dir_name):
			os.makedirs(pictures_dir_name)

		input_file_name = "scores_stat/" + c + "_users_scores_cut.tsv"
		input_file = open(input_file_name, "r")
	
		
	
		users_scores_percents = {}	 
	
		for line in input_file.readlines():
			tmp = line.strip().split("\t")
			tmp = map(int, tmp)	
			if tmp[0] < 0:
				continue	
			users_scores_percents[tmp[0]] = vecutils.normalize(tmp[1:])
		input_file.close()
	
		users = users_scores_percents.keys()
		scores_percents = users_scores_percents.values()
		vals = np.array(scores_percents)
		#print vals
		clusters = hcluster.fclusterdata(vals, d, depth = 1,  criterion='distance', metric ='euclidean', method='average')
		cluster_set = set(clusters) 
		print len(cluster_set) #_centroids
		g.write(str(d) + " " + str(len(cluster_set)) + "\n")
		cluster_elements = {}
		for cluster in cluster_set:
			cluster_elements[cluster] = []
		for (user, cluster) in zip(users, clusters):
			cluster_elements[cluster].append(user)
			
		f = open(output_dir_name + "/" + c + "_clusters_" + str(d) + ".txt", "w")	
		for (cluster, elements) in cluster_elements.items():
			tmp = " "
			if len(elements) < 10:
				tmp += str(elements)
			tmp = str(cluster) + " " + str(len(elements)) + tmp + "\n"
			f.write(tmp)
			#print tmp	
		f.close()
		print "Drawing pictures..."
		for i in xrange(0, len(scores)):
			for j in xrange(0, len(scores)):
				if i == j:
					continue
				plt.figure()
				plt.scatter(vals[:,i], vals[:,j], c = clusters, s = 100)
				plt.xlabel(scores[i])
				plt.ylabel(scores[j])
				pylab.savefig(pictures_dir_name + "/" + c + str(i) + str(j) + ".png")
				pylab.close() 
		print "Pictures drawn"
	g.close()
	
	
