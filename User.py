from Judgement import Judgement
import itertools


class User:
	def __init__(self, _id):
		self.id = _id
		self.judgements = []
		self.is_admin = False
		
		self.confusion_matrix = None
		self.precision = None
		self.recall = None
		
		
	def set_status(self, is_admin):
		self.is_admin = is_admin
		
	def add_judgement(self, judgement):
		self.judgements.append(judgement)
		
	def count_scores(self, scores):
		self.nscores = {}
		for sc in scores:
			self.nscores[sc] = 0
		for j in self.judgements:
			self.nscores[j.score] += 1
			
	def calc_confusion_matrix(self, scores, documents_real_scores):
		self.confusion_matrix = {}
		for psc in itertools.product(scores, scores):
			self.confusion_matrix[psc] = 0
		for jmt in self.judgements:
			doc_id = jmt.doc_id
			score = jmt.score
			if (doc_id in documents_real_scores.keys()):				
				psc = (score, documents_real_scores[doc_id])			
				self.confusion_matrix[psc] += 1
				
	def print_confusion_matrix(self, scores, f):
		
		for sc2 in scores:
			f.write(sc2 + " ")
		f.write("\n")
		for sc1 in scores:
			f.write(sc1 + " ")
			for sc2 in scores:
				f.write(str(self.confusion_matrix[(sc1, sc2)]) + " ")
			f.write("\n")	
		
			
	def calc_precision_and_recall(self, scores):
		self.precision = {}
		self.recall = {}
		for sc1 in scores: 
			tmp1 = 0
			tmp2 = 0
			for sc2 in scores:
				tmp1 += self.confusion_matrix[sc1, sc2]
				tmp2 += self.confusion_matrix[sc2, sc1]
			if (tmp1 == 0):
				self.precision[sc1] = 1
			else:
				self.precision[sc1] = float(self.confusion_matrix[sc1, sc1]) / tmp1
			if (tmp2 == 0):
				self.recall[sc1] = 1
			else:
				self.recall[sc1] = float(self.confusion_matrix[sc1, sc1]) / tmp2
	
	def average_precision(self):
		precisionvals = self.precision.values()
		p = float(sum(precisionvals)) / len(precisionvals)
		return p
		
	def average_recall(self):
		recallvals = self.recall.values()
		r = float(sum(recallvals)) / len(recallvals)
		return r
				
	def f_measure(self):
		p = self.average_precision()
		r = self.average_recall()
		if (p + r) > 0:
			f_measure = (2 * p * r) / (p + r)
			return f_measure
		else:
			return 0
		
		
		 
