from random import random, uniform
from math import sqrt

def normalize(v):
	s = sum(v)
	vlen = len(v)
	for i in xrange(0, vlen):
		v[i] = float(v[i])/s
	return v

def dist_2(v1, v2):
	res = 0
	vlen = len(v1)
	for i in xrange(0, vlen):
		tmp = v1[i] - v2[i]
		res += tmp * tmp
	res = sqrt(res)
	return res

def dist_1(v1, v2):
	res = 0
	vlen = len(v1)
	for i in xrange(0, vlen):
		tmp = abs(v1[i] - v2[i])
		if (tmp > res):
			res = tmp
	return res

def random_vec(vlen):
	v = []
	for i in xrange (0, vlen):
		v.append(uniform(0, 1))
	return v
	

	
