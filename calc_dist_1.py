import vecutils
import os


import pylab
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

countries = ["Russia", "Ukraine", "Belarus", "Kazakhstan","Turkey"]

for c in countries:
	print c
	input_file_name = "scores_stat/" + c + "_users_scores_cut.tsv"
	input_file = open(input_file_name, "r")
	
	output_dir_name = "dist_1"
	if not os.path.exists(output_dir_name):
			os.makedirs(output_dir_name)
	
	pictures_dir_name = output_dir_name + "/pictures"
	if not os.path.exists(pictures_dir_name):
			os.makedirs(pictures_dir_name)
	
	users_scores_percent = {}	 
	
	for line in input_file.readlines():
		tmp = line.strip().split("\t")
		tmp = map(int, tmp)		
		users_scores_percent[tmp[0]] = vecutils.normalize(tmp[1:])
	
	dist_from_avg_user = {}
	dist_from_avg_admin = {}
	for user_id in users_scores_percent.keys():
		if (user_id > 0):
			dist_from_avg_user[user_id] = vecutils.dist_1(users_scores_percent[user_id], users_scores_percent[-1])
			dist_from_avg_admin[user_id] = vecutils.dist_1(users_scores_percent[user_id], users_scores_percent[-2])
	sorted_dist_from_avg_user = sorted([(val,usr) for (usr,val) in dist_from_avg_user.items()])
	sorted_dist_from_avg_admin = sorted([(val,usr) for (usr,val) in dist_from_avg_admin.items()])
	print "Last 10 from average user:"
	print " ".join(str(usr) for (val, usr) in sorted_dist_from_avg_user[-10:])
	
	print "Last 10 from average admin:"
	print " ".join(str(usr) for (val, usr) in sorted_dist_from_avg_admin[-10:])
	
	plt.figure()
	plt.hist(dist_from_avg_user.values())
	pylab.savefig(pictures_dir_name + "/" + c + "_dist_from_avg_usr.png")
	pylab.close()
	
	plt.figure()
	plt.hist(dist_from_avg_admin.values())
	pylab.savefig(pictures_dir_name + "/" + c + "_dist_from_avg_adm.png")
	pylab.close()
	
	f = open(output_dir_name + "/" + c + "_sorted_dist_from_avg_usr.tsv", "w")
	g = open(output_dir_name + "/" + c + "_sorted_dist_from_avg_adm.tsv", "w")
	for (val, usr) in sorted_dist_from_avg_user:
		f.write(str(usr) + "\t" + str(val) + "\n")
	f.close()
	
	for (val, usr) in sorted_dist_from_avg_admin:
		g.write(str(usr) + "\t"  + str(val) + "\n")
	g.close()

