from Enumerator import Enumerator

from math import log10

import os

import pylab
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab


	
	
	

countries = ["Russia", "Ukraine", "Belarus", "Kazakhstan","Turkey"]	
scores = ['VITAL_CANDIDATE', 'USEFUL', 'RELEVANT_PLUS', 'RELEVANT_MINUS', 'IRRELEVANT', '_404', 'VIRUS']


output_dir_name = "scores_stat"
if not os.path.exists(output_dir_name):
	os.makedirs(output_dir_name)
log_file = open(output_dir_name + "/" + "log.txt", "a")

for c in countries:

	print c
	njudgements = []
	
	enumerator = Enumerator("countries/" + c + ".tsv")
		
	users_enum = enumerator.create_users_enum()
	users_ids = users_enum.keys()
	users_ids.sort()
	
	
	output_file = open(output_dir_name + "/" + c + "_users_scores.tsv", "w")	
	for usr_id in users_ids:
		user = users_enum[usr_id]
		output_file.write(str(user.id) + "\t" + str(user.is_admin) + "\t")		
		user.count_scores(scores)		
		for sc in scores:
			output_file.write(str(user.nscores[sc]) + "\t")
		s = sum(user.nscores.values())
		njudgements.append(s)
		output_file.write("\n")
	output_file.close()
	
	
	pictures_dir_name = output_dir_name + "/pictures"
	if not os.path.exists(pictures_dir_name):
			os.makedirs(pictures_dir_name)
	
	log_njudgements = map(log10, njudgements)
	print "Number of users:" + str(len(njudgements))
	log_file.write(c + str(len(njudgements)))
	m = int(max(log_njudgements)) + 1
	
	plt.figure()
	plt.hist(log_njudgements, bins = range(0, m + 2))
	pylab.savefig(pictures_dir_name + "/" + c + "_lognjudgements.png")
	pylab.close()
log_file.close()
	
