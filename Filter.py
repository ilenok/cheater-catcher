import fileinput
import strutils
import os 
from datetime import datetime
from Entry import Entry

class Filter:	
	
	def __init__(self):	
				
		'''self.task_ids = None
		self.doc_ids = None
		self.user_ids = None
		self.scores = None
		self.start_date_time = None
		self.end_date_time = None
		self.min_user_precision = None
		self.max_user_precision = None
		self.is_checking = None
		self.countries = None
		self.is_admin = None'''
		
		self.input_file_name = None
		self.output_dir_name = None
		self.output_file = None
		self.log_file = None
		
	
	def set_input_file_name(self, input_file_name):
		self.input_file_name = input_file_name	
		
	def filter_by_task_id(self, task_ids):
		i = 0
		k = 0		
		for line in fileinput.input(self.input_file_name):			
			if  strutils.emptystr(line):
				continue									
			entry = Entry(line)	
			if (entry.task_id in task_ids):
				self.output_file.write(line)
				k += 1
			i += 1						
			if (i % 1000000 == 0):
				print i	/ 1000000
		self.log_file.write("Tasks " + ", ".join(task_ids) + ": " + str(k) + "\n")
	
	def filter_by_doc_id(self, doc_ids):
		i = 0
		k = 0		
		for line in fileinput.input(self.input_file_name):			
			if  strutils.emptystr(line):
				continue									
			entry = Entry(line)	
			if (entry.doc_id in doc_ids):
				self.output_file.write(line)
				k += 1
			i += 1						
			if (i % 1000000 == 0):
				print i	/ 1000000
		self.log_file.write("Docs " + ", ".join(doc_ids) + ": " + str(k) + "\n")			
					
	def filter_by_user_id(self, user_ids):
		i = 0	
		k = 0	
		for line in fileinput.input(self.input_file_name):			
			if  strutils.emptystr(line):
				continue										
			entry = Entry(line)	
			if (entry.user_id in user_ids):
				self.output_file.write(line)
				k += 1
			i += 1						
			if (i % 100000 == 0):
				print i	/ 100000
		self.log_file.write("Users " + ", ".join(user_ids) + ": " + str(k) + "\n")	
	
	def filter_by_score(self, scores):
		i = 0
		k = 0		
		for line in fileinput.input(self.input_file_name):			
			if  strutils.emptystr(line):
				continue										
			entry = Entry(line)	
			if (entry.score in scores):
				self.output_file.write(line)
				k += 1
			i += 1						
			if (i % 100000 == 0):
				print i	/ 100000
		self.log_file.write("Scores " + ", ".join(scores) + ": " + str(k) + "\n")
	
	def filter_by_reliance(self, min_reliance, max_reliance):
		i = 0	
		k = 0	
		for line in fileinput.input(self.input_file_name):			
			if  strutils.emptystr(line):
				continue										
			entry = Entry(line)	
			if (entry.reliance >= min_reliance) and (entry.reliance <= max_reliance):
				self.output_file.write(line)
				k += 1
			i += 1						
			if (i % 100000 == 0):
				print i	/ 100000
			self.log_file.write("Reliance "  + str(min_reliance) + "<= r <= " + str(max_reliance) + ": " + str(k) + "\n")	
				
	def filter_if_checking(self, is_checking):
		i = 0	
		k = 0	
		for line in fileinput.input(self.input_file_name):			
			if  strutils.emptystr(line):
				continue										
			entry = Entry(line)	
			if (entry.is_checking == is_checking): 
				self.output_file.write(line)
				k += 1
			i += 1						
			if (i % 100000 == 0):
				print i	/ 100000
		if (is_checking):
			self.log_file.write("Checking: "  + str(k) + "\n")
		else:
			self.log_file.write("Not checking: "  + str(k) + "\n")
				
	def filter_by_country(self, countries):
		i = 0
		k = 0
		for line in fileinput.input(self.input_file_name):			
			if  strutils.emptystr(line):
				continue										
			entry = Entry(line)	
			if (entry.country in countries.keys()):
				self.output_file.write(line)
				k += 1
			i += 1						
			if (i % 100000 == 0):
				print i	/ 100000
		self.log_file.write("Countries " + ", ".join(countries.values()) + ": " + str(k) + "\n")
				
	def filter_if_admin(self, is_admin):
		i = 0		
		for line in fileinput.input(self.input_file_name):			
			if  strutils.emptystr(line):
				continue										
			entry = Entry(line)	
			if (entry.is_admin == is_admin): 
				self.output_file.write(line)
			i += 1						
			if (i % 100000 == 0):
				print i	/ 100000
		if (is_admin):
			self.log_file.write("Admins' judgements: "  + str(k) + "\n")
		else:
			self.log_file.write("Not admins ' judgements: "  + str(k) + "\n")
	
	def create_output_files(self, output_dir_name, output_file_name, log_file_name):
		if not os.path.exists(output_dir_name):
			os.makedirs(output_dir_name)
			self.output_dir_name = output_dir_name
		self.output_file = open(output_dir_name + "/" + output_file_name, "w")
		self.log_file = open(output_dir_name + "/" + log_file_name, "a")
	
	def write_to_log(line):
		self.log_file.write(line)		
			
	def close_output_files(self):
		self.output_file.close()
		self.log_file.close()	






