from Entry import Entry
from Judgement import Judgement
from User import User
from Enumerator import Enumerator

import fileinput
import os
import strutils

from math import log10

import pylab
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab



countries = ["Russia", "Ukraine", "Belarus", "Kazakhstan","Turkey"]	
scores = ['VITAL_CANDIDATE', 'USEFUL', 'RELEVANT_PLUS', 'RELEVANT_MINUS', 'IRRELEVANT', '_404', 'VIRUS']


for c in countries:

	print c
	njudgements = []
	
	enumerator = Enumerator("control/" + c + "_control.tsv")
		
	users_enum = enumerator.create_users_enum()
	users_ids = users_enum.keys()
	users_ids.sort()
		
	docs_real_scores = enumerator.create_docs_enum()
	
	input_file = open("scores_stat/" + c + "_above_threshold.txt")
	users_ids_above_threshold = map(int, input_file.readline().split())
	users_ids_above_threshold = set(users_ids_above_threshold)
	
	output_dir_name = "confusion"
	if not os.path.exists(output_dir_name):
			os.makedirs(output_dir_name)
		
	output_file_1 = open(output_dir_name + "/" + c + "_confusion_matrices.txt", "w")
	output_file_2 = open(output_dir_name + "/" + c + "_precision_recall_all.tsv", "w")
	output_file_3 = open(output_dir_name + "/" + c + "_precision_recall.tsv", "w")
	output_file_4 = open(output_dir_name + "/" + c + "_f_measure.tsv", "w")	
	for usr_id in users_ids:
		if usr_id not in users_ids_above_threshold:
			continue
		user = users_enum[usr_id]
		if (user.is_admin):
			continue
		print usr_id, "Number of judgements:" + str(len(user.judgements))
		user.calc_confusion_matrix(scores, docs_real_scores)
		user.calc_precision_and_recall(scores)
		
		output_file_1.write(str(usr_id) + "\n")
		user.print_confusion_matrix(scores, output_file_1)
		output_file_2.write(str(usr_id) + "\n")
		for sc in scores:
			output_file_2.write(str(user.precision[sc]) + "\t")
		output_file_2.write("\n")
		for sc in scores:
			output_file_2.write(str(user.recall[sc]) + "\t")
		output_file_2.write("\n")
		output_file_3.write(str(usr_id) + "\t")
		output_file_3.write(str(user.average_precision()) + "\t" + str(user.average_recall()) + "\n")
		output_file_4.write(str(usr_id) + "\t" + str(user.f_measure()) + "\n")	
	output_file_1.close()
	output_file_2.close()
	output_file_3.close()
	output_file_4.close()
			
	'''		
	output_file = open(output_dir_name + "/" + c + "_confusion.tsv", "w")	
	
	for usr_id in users_ids:
		user = users_enum[usr_id]
		output_file.write(str(user.id) + "\t" + str(user.is_admin) + "\t")		
		user.count_scores(scores)		
		for sc in scores:
			output_file.write(str(user.nscores[sc]) + "\t")
		s = sum(user.nscores.values())
		njudgements.append(s)
		output_file.write("\n")
	output_file.close()
	
	pictures_dir_name = output_dir_name + "/pictures"
	if not os.path.exists(pictures_dir_name):
			os.makedirs(pictures_dir_name)
	
	log_njudgements = map(log10, njudgements)
	print "Number of users:" + str(len(njudgements))
	m = int(max(log_njudgements)) + 1
	
	plt.figure()
	plt.hist(log_njudgements, bins = range(0, m + 2))
	pylab.savefig(pictures_dir_name + "/" + c + "_lognjudgements.png")

	'''
