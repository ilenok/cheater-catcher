import vecutils
import os

import numpy as np
import scipy.cluster.vq as scicluster


import pylab
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab


#countries = ["Ukraine"]

countries = ["Russia", "Ukraine", "Belarus", "Kazakhstan","Turkey"]
scores = ['VITAL_CANDIDATE', 'USEFUL', 'RELEVANT_PLUS', 'RELEVANT_MINUS', 'IRRELEVANT', '_404', 'VIRUS']

for k in xrange(1, 11):
	for c in countries:
		print c, str(k), "clusters"
		input_file_name = "scores_stat/" + c + "_users_scores_cut.tsv"
		input_file = open(input_file_name, "r")
	
		output_dir_name = "clustering/kmclusters" 
		if not os.path.exists(output_dir_name):
				os.makedirs(output_dir_name)
	
		pictures_dir_name = output_dir_name + "/pictures" + str(k)
		if not os.path.exists(pictures_dir_name):
				os.makedirs(pictures_dir_name)
	
		users_scores_percents = {}	 
	
		for line in input_file.readlines():
			tmp = line.strip().split("\t")
			tmp = map(int, tmp)	
			if tmp[0] < 0:
				continue	
			users_scores_percents[tmp[0]] = vecutils.normalize(tmp[1:])
		input_file.close()
	
		users = users_scores_percents.keys()
		scores_percents = users_scores_percents.values()
		vals = np.array(scores_percents)
		if (k == 0):
			it = 10
		else:
			it = 30
		cluster_centroids, closest_centroids = scicluster.kmeans2(vals, 5, iter = it, minit = 'points')
	
		#print cluster_centroids
	
		users_dists_from_cluster_center = {}
		users_centroids_ids = dict(zip(users, closest_centroids))
		#print users_clusters_centers
		for user in users_centroids_ids.keys():
			centroid_id = users_centroids_ids[user]
			d = vecutils.dist_2(users_scores_percents[user], cluster_centroids[centroid_id])
			users_dists_from_cluster_center[user] = d
		#print users_dists_from_cluster_center
		
		sorted_dists_from_cluster_center = sorted([(val,usr) for (usr,val) in users_dists_from_cluster_center.items()])
		#print sorted_dists_from_cluster_center
	
		plt.figure()
		plt.hist(users_dists_from_cluster_center.values())
		pylab.savefig(pictures_dir_name + "/" + c + "_dists_from_cluster_center.png")
		pylab.close()

		f = open(output_dir_name + "/" + c  + "_sorted_dists_from_cluster_center" +  "_" + str(k) + ".tsv", "w")
	
		for (val, usr) in sorted_dists_from_cluster_center:
			f.write(str(usr) + "\t" + str(val) + "\n")
		f.close()
	
		print "Drawing pictures..."
		for i in xrange(0, len(scores)):
			for j in xrange(0, len(scores)):
				if i == j:
					continue
				plt.figure()
				plt.scatter(vals[:,i], vals[:,j], c = closest_centroids, s = 100)
				plt.xlabel(scores[i])
				plt.ylabel(scores[j])
				pylab.savefig(pictures_dir_name + "/" + c + str(i) + str(j) + ".png")
				pylab.close() 
		print "Pictures drawn"
	print "All countries", str(k), "clusters -- done" 
	
	
