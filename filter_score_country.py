from Filter import Filter


my_filter = Filter()

print "Filtering by score..."
scores = ['VITAL_CANDIDATE', 'USEFUL', 'RELEVANT_PLUS', 'RELEVANT_MINUS', 'IRRELEVANT', '_404', 'VIRUS']

my_filter.set_input_file_name("judgements2.tsv")
my_filter.create_output_files("tmp", "judgements_relevance.tsv", "log.txt")
my_filter.filter_by_score(scores)
my_filter.close_output_files()


print "Filtering by country..."
all_countries = {1: "Russia", 2: "Ukraine", 3: "Belarus", 4: "Kazakhstan", 8: "Turkey"}
my_filter.set_input_file_name("tmp/judgements_relevance.tsv")
for c in all_countries:
	print all_countries[c]
	my_filter.create_output_files("countries", all_countries[c] + ".tsv", "log.txt")
	my_filter.filter_by_country({c: all_countries[c]})
	my_filter.close_output_files()
	
	
