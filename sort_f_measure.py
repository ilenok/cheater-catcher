import vecutils
import os


import pylab
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab


countries = ["Russia", "Ukraine", "Belarus", "Kazakhstan","Turkey"]	

for c in countries:

	print c
	user_f_measure = {}
	input_file = open("confusion/" + c + "_f_measure.tsv", "r")
	for line in input_file.readlines():
		user_id, f_measure = line.split()
		user_id = int(user_id)
		f_measure = float(f_measure)
		user_f_measure[user_id] = f_measure
	sorted_f_measure = sorted([(val,usr) for (usr,val) in user_f_measure.items()])
	
	output_dir_name = "confusion"
	if not os.path.exists(output_dir_name):
			os.makedirs(output_dir_name)
			
	pictures_dir_name = output_dir_name + "/pictures"
	if not os.path.exists(pictures_dir_name):
			os.makedirs(pictures_dir_name)
	
	print "Worst 10 by F-measure:"
	print " ".join(str(usr) for (val, usr) in sorted_f_measure[:10])
	
	output_file = open(output_dir_name + "/" + c + "_sorted_f_measure.tsv", "w")
	
	for (val, usr) in sorted_f_measure:
		output_file.write(str(usr) + "\t" + str(val) + "\n")
	output_file.close()		
	
	plt.figure()
	plt.hist(user_f_measure.values())
	pylab.savefig(pictures_dir_name + "/" + c + "_f_measure.png")	 
	
	
