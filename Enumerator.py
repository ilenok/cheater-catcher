from Judgement import Judgement
from Entry import Entry
from User import User
import fileinput
import strutils


class Enumerator:
	def __init__(self, input_file_name):
		self.input_file_name = input_file_name
		
	def create_docs_enum(self):
		i = 0	
		docs_real_scores = {}	
		
		for line in fileinput.input(self.input_file_name):			
			if  strutils.emptystr(line):
				continue												
			entry = Entry(line)
			if (entry.is_admin == True):		
				if (entry.doc_id in docs_real_scores.keys()): 
					pass
				else:
					docs_real_scores[entry.doc_id] = entry.score	
			i += 1
			if (i % 100000 == 0):
					print i	/ 100000
		
		return docs_real_scores

	def create_users_enum(self):
		i = 0
	
		users = {}	
		
		for line in fileinput.input(self.input_file_name):			
			if  strutils.emptystr(line):
				continue
												
			entry = Entry(line)
			judgement = Judgement(entry)	
			if (entry.user_id in users.keys()): 
				pass
			else:
				users[entry.user_id] = User(entry.user_id)
				users[entry.user_id].set_status(entry.is_admin)
			users[entry.user_id].add_judgement(judgement)
		
			i += 1
			if (i % 100000 == 0):
					print i	/ 100000
		
		return users
