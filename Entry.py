from datetime import datetime
import re

class Entry:
	
	'''
	task_id
	doc_id
	user_id
	score
	time
	user_precision
	is_checking
	country
	is_admin
	
	'''

	def __init__(self, line):
		tmp = re.split(r"\t+", line)						
		self.task_id, self.doc_id, self.user_id = map(int, tmp[0:3])
		score_str, time_str, reliance_str, is_checking_str, country_str, is_admin_str = tmp[3:]
		self.score = score_str
		self.time = datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")
		self.reliance = float(reliance_str)
		self.is_checking = bool(int(is_checking_str))	
		self.country = int(country_str)
		self.is_admin = bool(int(is_admin_str))
		
		
