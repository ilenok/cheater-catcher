
	
scores = ['VITAL_CANDIDATE', 'USEFUL', 'RELEVANT_PLUS', 'RELEVANT_MINUS', 'IRRELEVANT', '_404', 'VIRUS']

threshold = {"Russia": 1000, "Ukraine": 1000, "Belarus": 1000, "Kazakhstan": 1000,"Turkey": 1000}	

for c in threshold.keys():
	print c
	input_file_name = "scores_stat/" + c + "_users_scores.tsv"
	input_file = open(input_file_name, "r")
	
	njudgements = []
	users_nscores = {}
	
	total_user_nscores = [0] * len(scores) 
	total_admin_nscores = [0] * len(scores) 
	
	for line in input_file.readlines():
		tmp = line.strip().split("\t")
		
		user_id = int(tmp[0])
		if (tmp[1] == "True"):
			is_admin = True
		else:
			is_admin = False
		
		nscores	= map(int, tmp[2:])
		
		for i in xrange(0, len(scores)):
			total_user_nscores[i] += nscores[i] 
		if (is_admin):
			for i in xrange(0, len(scores)):
				total_admin_nscores[i] += nscores[i]
		s = sum(nscores)
		njudgements.append(s)
		if (s < threshold[c]):
			pass
		else:
			users_nscores[user_id] = nscores
			
	print "Above threshold: " + str(len(users_nscores))	
	
			
	output_file_1 = open("scores_stat/" + c + "_users_scores_cut.tsv", "w")
	output_file_2 = open("scores_stat/" + c + "_above_threshold.txt", "w")
	output_file_1.write("-1\t" + "\t".join(map(str,total_user_nscores)) + "\n") #average user
	output_file_1.write("-2\t" + "\t".join(map(str,total_admin_nscores)) + "\n")	#average admin
	
	for user_id in users_nscores:
		output_file_1.write(str(user_id) + "\t" + "\t".join(map(str, users_nscores[user_id]))+ "\n")
		output_file_2.write(str(user_id) + " ")
	output_file_1.close()
	output_file_2.close()
	
	
	
